# API Management #

Providing APIs for consumption constitute a long-term commitment of maintaining structure and stable semantics. Therefore, it is vital that PostNord exposes the API functionality in a controlled and standardized manner, in order to promote ease of understanding, communication, development and usage. 

- The API strategy is to provide more and more valuable business data in public, private and internal APIs.

**This is a placeholder for sharing documents to have that approach (vs ad hoc) to ensure that the following is accomplished;**

- Consistency across all APIs
- Established process of publishing, promoting, and overseeing APIs in a secure and scalable environment.
- Ensure that developers and partners are productive.
- Managing, securing and mediating the API traffic.
- Allow APIs to easily evolve overtime (grow)
- Enable monetization of APIs
- Self-service and automated approach  

### API checklist

All public API has to be re-reviewed by the API GOV , according to the following;

- Developed after PostNord Guidelines
- URI naming convention of the API endpoint
- Swagger 2.0 definition exists
- Use PostNord API information model and standard error objects
  - [PostNord Information Model ](https://app.swaggerhub.com/domains/postnord/PostNord-Information-Model/1.0.0)
  - [PostNord Response Codes & Error model](https://app.swaggerhub.com/domains/postnord/PostNord-Response-Codes/1.0.0)
- Surveillance setup (the API must have a health endpoint)
- Performance test report
- Security report (if applicable)

### Links

- [Postnord Developer Portal @PROD](https://developer.postnord.com/)
- [Postnord API Status Dashboard](http://apistatus.postnord.com/apistatus/#/overview)
- [Swagger Online Editor](http://swagger.io/swagger-editor/)
- [JSON Web Token](https://jwt.io/) 


### ToDos

- Postnord Authentication Guidelines
- Postnord Swagger Guidelines

### Contact

- Email: <api.support@postnord.com>
- Product Owner: <staffan.asberg@postnord.com>
- Architect: <fredrik.bodin@postnord.com>