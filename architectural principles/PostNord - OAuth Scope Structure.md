# PostNord - OAuth Scope Structure

> Scope is a way to limit a client access to a resource. Rather than granting complete access to the resource, is it useful to give clients a way to request a more limited scope of what they are allowed to do on behalf of a user. 

PostNord scopes are defined as URI:s

**URI Structure**

```
https://api.postnord.com/scopes/{context}/{resource}/{value}
```

> The swagger for the API, must define the required "scopes" to resources

**{context}**

> Groups the APIs according to their main information/business concept that is subject to the provided functionality. Refers to the exposed API URI endpoint.

**{resource}**

> Primary data representation; can be a singleton or a collection and may contain sub-collection resources.

**{value}**
The final part of the URI is resource specific. Some common examples:

| Definition | Grants                                                       |
| ---------- | ------------------------------------------------------------ |
| all        | Full access                                                  |
| write      | Modifying the resource in any way e.g. creating, editing, or deleting |
| read       | Reading the full information about a single resource         |
| {customs}  | Functionality oriented (compose, mail, etc)                  |
| (no scope) | No access                                                    |

**Examples**

```
https://api.postnord.com/scopes/shipment/itemchange/write
https://api.postnord.com/scopes/shipment/search/recipient
```
