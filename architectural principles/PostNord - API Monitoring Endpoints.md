# PostNord - API Monitoring Endpoints

**Expose operational information about the running application** – health, metrics, info, dump, environment, etc.

Here are some of the most common endpoints:

- ***<resource>/manage/health –*** Shows application health information (a ***status*** when accessed over an unauthenticated connection or full message details when authenticated); it’s not sensitive by default
- ***<resource>/manage/metrics –*** Shows ‘metrics’ information for the current application; it’s also sensitive by default
- **<resource>/manage/info –*** Displays arbitrary application info; not sensitive by default
- ***<resource>/manage/trace –*** Displays trace information (by default the last few HTTP requests)

> The Monitoring our app, gathering metrics, understanding traffic and the state of our technical dependency are important.

---

### Health endpoint (Mandatory)

**The /health endpoint is used to check the health or state of the running application.** It’s usually exercised by monitoring software to alert us if the running instance goes down or gets unhealthy for other reasons. e.g. connectivity issues with our DB, lack of disk space…

By default only health information is shown to unauthorized access over HTTP:

```json
{
  "status": "UP",
  "detailChecks": [
    {
      "status": "UP",
      "name": "Disk space",
      "check": {
        "param": "string",
        "value": "string"
      }
    }
  ]
}
```

This health information is collected from the logical implementation of the component, and should be  exposed under the API context: /<resource>/manage/health 

```yaml
# Health object, should be placed under paths: /<resource>/manage/health  
  health:
    description: Response on API health check (UP=OK, FATAL=Needs action, DOWN=Not working)
    type: object
    required:
      - status
    properties:
      status:
        type: string
        enum:
          - UP
          - FATAL
          - DOWN
      detailChecks:
        type: array
        items:
          $ref: '#/definitions/detailCheck'

# detailCheck object
  detailCheck:
    description: Detailed health check 
    type: object
    required:
      - status
    properties:
      status:
        type: string
        enum:
          - UP
          - FATAL
          - DOWN
      name: 
        type: string
        description: 'The name associated with the check'
      check:
          $ref: '#/definitions/paramValue'
          
# PostNord standardobject
  paramValue:
    description: A parameter value pair is a set of two linked data items
    type: object
    properties:
      param:
        type: string
      value:
        type: string
```

A **custom health indicator** can be implemented which can collect any type of custom health data specific to the application and automatically expose it through the */health* endpoint (the /metric endpoint can also be for this purpose).

---

### **Metrics Endpoint**

**The metrics endpoint publishes information about OS, JVM as well as application level metrics**. Here can you visualize information such as memory, heap, processors, threads, classes loaded, classes unloaded, thread pools along with some HTTP metrics as well.

Here’s what the output of this endpoint looks like out of the box:

```json
{
    "mem" : 193024,
    "mem.free" : 87693,
    "processors" : 4,
    "instance.uptime" : 305027,
    "uptime" : 307077,
    "systemload.average" : 0.11,
    "heap.committed" : 193024,
    "heap.init" : 124928,
    "heap.used" : 105330,
    "heap" : 1764352,
    "threads.peak" : 22,
    "threads.daemon" : 19,
    "threads" : 22,
    "classes" : 5819,
    "classes.loaded" : 5819,
    "classes.unloaded" : 0,
    "gc.ps_scavenge.count" : 7,
    "gc.ps_scavenge.time" : 54,
    "gc.ps_marksweep.count" : 1,
    "gc.ps_marksweep.time" : 44,
    "httpsessions.max" : -1,
    "httpsessions.active" : 0,
    "counter.status.200.root" : 1,
    "gauge.response.root" : 37.0
}
```

---

**Info Endpoint**

We can also customize the data shown by the */info* endpoint and the sample output:

```json
{
  "git" : {
    "commit" : {
      "time" : "+50132-12-21T14:02:17Z",
      "id" : "df027cf"
    },
    "branch" : "master"
  },
  "build" : {
    "version" : "1.0.3",
    "artifact" : "application",
    "group" : "com.example"
  }
}
```

**Trace Endpoint**

The trace endpoint provides information about HTTP request-response exchanges.

Sample output:

```json
{
  "traces" : [ {
    "timestamp" : "2018-03-01T03:56:59.402Z",
    "principal" : {
      "name" : "alice"
    },
    "session" : {
      "id" : "ee4f3e4e-9030-4ad0-9afa-8ea779816024"
    },
    "request" : {
      "method" : "GET",
      "uri" : "https://api.example.com",
      "headers" : {
        "Accept" : [ "application/json" ]
      }
    },
    "response" : {
      "status" : 200,
      "headers" : {
        "Content-Type" : [ "application/json" ]
      }
    },
    "timeTaken" : 2
  } ]
}
```

