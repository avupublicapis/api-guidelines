# PostNord - Public API Naming Convention

**Structure**

```
https://{host}/{tag}/{route}/{context}/{vN}/{resource}
```

**{host}**

> Host is the domain name of the host that serves the API

| host              | description                                                |
| ----------------- | ---------------------------------------------------------- |
| api2.postnord.com | Public, serves restFul APIs, requires the party to sign up |
| api3.postnord.com | Public, serves SOAP APIs                                   |

**{tag}**

> Define the API technology `/rest` or `/soap` 

**{route}**

> Use these values, if applicable otherwise create a new one  `rsap (sap), rcnt (cint),raws (aws), rslf (salesforce), rnva (nova), risp (isp), rswo (sweco)`

**{vN}**

> Defines the major version of the API `e.g. /v1` , the API must be able to evolve overtime to minimize the need for new versions.

**{context}**

> Groups the APIs according to their main information/business concept that is subject to the provided functionality. This also works as an hierarchy level in the developer portal, to make it easier presenting the APIs business domain. 
>
> New public contexts must be approved by the api.support@postnord.com

| context   | Description                                             |
| --------- | ------------------------------------------------------- |
| shipment  | Related to shipments; track, collect code, flex, change |
| location  | Related to service points, address, mailboxes           |
| transport | Related to timeslot, pickups, transit time              |
| customer  | Related to customers, recipients, TA-suppliers          |

**{resource}**

> Primary data representation; can be a singleton or a collection  and may contain sub-collection resources.

**Examples**

```
{host}/rest/customer/v1/identity/101
{host}/rest/shipment/v2/trackandtrace/xxxxxSE
{host}/rest/location/v1/address/search
{host}/rest/rswo/location/v1/address/search
{host}/rest/transport/v1/order/ruralpostman/delivery
{host}/rest/transport/v2/transit/time
{host}/rest/risp/transport/v2/transit/time
```

