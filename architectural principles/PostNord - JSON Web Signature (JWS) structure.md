# PostNord - JSON Web Signature (JWS) structure

All PostNord restful APIs that needs to be secure, must use a JWS.

> JSON Web Token (JWT) is a compact, URL-safe means of representing claims to be transferred between two parties.  The claims in a JWT are encoded as a JSON object that is used as the payload of a JSON Web Signature (JWS) structure or as the plaintext of a JSON Web   Encryption (JWE) structure, enabling the claims to be digitally signed or integrity protected with a Message Authentication Code (MAC) and/or encrypted.

| attribute                          | example                                                | description                                                  |
| ----------------------------------- | :----------------------------------------------------- | :----------------------------------------------------------- |
| typ                                | JWT                                                        | Header: Declaring the type                                  |
| kid                                | MPLju2weiUjrBcqZsfnMCAbZ988wp3NH-gc3qOWxyYc                | Header: Indicating which key was used to secure the JWS     |
| alg                                | RS256                                                      | Header: Hashing algorithm                                   |
| sub                                | erik.ekstrom@posten.se                                     | The ID used in the login by the end-user |
| iss                                | https://account.postnord.com                               | The authorization server used in the login. |
| access_token	                     | mbC7yaLh5ZnwO8NCVQ3x                                       | Secret reference that lets you act on a specific users behalf. Used against the UserInfo endpoint |
| scope                              | https://api.postnord.com/scopes/shipment/declare/vat/write | Lists scopes attached to the token that were granted by the user |
| exp                                | 1504682396                                                 | Defines the expiration in numeric Date                       |
| iat                                | 1504678803                                                 | The time the JWT was issued                                  |
| user_id 							 | claims-public                                              | Global unique IAM user id, if it exists          			|
| email                              | vijil.girish@omegapoint.se                                 | The user's email address |
| email_verified                     | true                                                       | True if the user's e-mail address has been verified; otherwise false |
| phone_number                       | 0123456789                                                 | The user's phone number  |
| phone_number_verified              | true                                                       | True if the user's phone number has been verified; otherwise false |
| https://api.postnord.com/claims/shipmentIds | 01305093792048                                    | Scope that grants privileges to the shipment Ids	|
| personnumber		                 | 199909098888                                               | The personal number associated with the user          |
| personnumber_country_code	         | SE                                                         | The user country associated with the personal number  |
| registered_customer_id_list        | 10422118                                                   | The legal entity associated with the user				|
