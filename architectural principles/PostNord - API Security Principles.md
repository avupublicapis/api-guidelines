# PostNord - API Security Principles

> The security principles applies for public and internal APIs.
>

* **Avoid exposing API endpoints to the Internet, if possible**
* **Use HTTPS as communication protocol**
* **Use the PostNord Identity Access Management (IAM) for authentication and federation of users**
	* *Trusts can be setup to other users federation, if required*
* **Use [OAuth](https://tools.ietf.org/html/rfc6749)[ 2.0](https://tools.ietf.org/html/rfc6749) framework for authentication**
	* *Issues an **Access Token** that withholds the credential*
	* *The Access Token should be used as a **Bearer** credential and sent in an HTTP **Authorization** header to the API*
	* *The **API Gateway** preforms an **introspect** to the resource server and creates an **JWT** of the Access token* 
* **Use [OpenID](http://openid.net/connect/) Connect when the client needs end-user information (ID-token)**
	* *An **ID token** is generated with end-user information based on the authentication*
	* *Used to **authenticate end-users across websites and apps** without having to own their authentication credentials*
	* *ID token **should only** be used by the client*
	* *ID token attributes can be used as query parameters in API calls*
* **Use a JSON Web Token (JWT), if a restful APIs needs to be secured**
	* *The JWT (JWS) is based on an OAuth2 Access-token*
* **Use [PostNord - OAuth Scope Structure](https://bitbucket.org/avupublicapis/api-guidelines/src/d13ead32f51846f5cdd7acab9c327622b09f4ead/architectural%20principles/PostNord%20-%20OAuth%20Scopes%20Structure.pdf?at=master); when defining scopes for APIs**
	* *The scope structure must be aligned and consistent over all APIs*
* **The restful API access control list must at least verify;**
	* *JWT origins*, *Expiration time*, *Listed scopes*
* **The restful API must [rotate the signing keys](https://openid.net/specs/openid-connect-core-1_0-final.html#RotateSigKeys), used to decrypt the JWT** 
	* *Common CA lifecycle management* 
* **Use the UserInfo endpoint**
	* *To retrieve authorized information about the End-User*
	* *Use the access token as reference ID*
* **Restrict HTTP verb methods**
	* *Apply a whitelist of permitted HTTP Methods e.g. GET, POST, PUT*
* **Input validations**
	* *Do not trust input parameters/objects*
* **Validate content types**
	* *The request/response body should match the intended type in the Header*
* **Respond with generic error messages**
	* *Avoid revealing details of the failure unnecessarily*
* **Audit log**
	* *Write audit logs before and after security related events*


