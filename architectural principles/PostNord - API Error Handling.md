# PostNord - API Error Handling

An important aspect of the API building, is to handle handle errors in a good ways. 



[TOC]





### API - Common Fault Object

Postnord has created  a common fault object and the principles below.

---

#### Swagger definition

```yaml
# PostNord standard fault object build up
  paramValue:
    description: A parameter value pair is a set of two linked data items
    type: object
    properties:
      param:
        type: string
      value:
        type: string
  fault:
    description: Fault object with the associated code and explanation text
    type: object
    required:
      - explanationText
    properties:
      paramValues:
        type: array
        uniqueItems: true
        items:
          $ref: '#/definitions/paramValue'
      explanationText:
        type: string
        example: Missing parameter
      faultCode:
        type: string
        example: API-005
  compositeFault:
    description: The composite fault object containing an array of fault objects
    type: object
    properties:
      faults:
        type: array
        uniqueItems: true
        items:
          $ref: '#/definitions/fault'
  errorResponse:
    description: PostNord standard error response message
    type: object
    required:
      - message
    properties:
      compositeFault:
        $ref: '#/definitions/compositeFault'
      message:
        type: string
        example: Query parameter missing
        description: High level error message.

```

#### JSON representation

```json
{
  "compositeFault": {
    "faults": [
      {
        "paramValues": [
          {
            "param": "param",
            "value": "value"
          }
        ],
        "explanationText": "Missing parameter",
        "faultCode": "API-005"
      }
    ]
  },
  "message": "Query parameter missing"
}
```

### Response Error Principles

There are several ways to implement, how an API should respond to different error scenarios.

#### SECURITY

- Do not return a **param** and **value** that are classified as a security risk.

#### COMMON

***Never return "500 - Internal Server Error" intentionally***

- The general catch-all error when the server-side throws an exception

#### POST

***Create new resource***

- "201 Created" + response body on resource creation (shall also include server generated values)

#### GET

***List a collection***

- If the collection is empty (0 items in response), "404 Not Found" is not appropriate. 
- The items array should just be empty, and collection metadata fields provided (e.g. "total_count": 0). 
- Invalid query parameter values can result in "400 Bad Request" 
- Otherwise "200 OK" is utilized for a successful response.

***Single resource***

- If the provided resource identifier is not found, responds "404 Not Found" HTTP status
- Otherwise, "200 OK" HTTP status should be utilized when data is found.

#### PUT/PATCH/DELETE

***Single resource***

- Any failed request validation responds "400 Bad Request" + provide a specific error in the common fault object 
- If clients attempt to modify read-only fields, this is also a "400 Bad Request"
- If there are business rules, provide a specific error in common fault object for that validation.
- If the resource doesn't exist return "404 Not Found"
- After successful update, PUT operations should respond with "204 No Content" status, with no response body.

---

### Fault Code

PostNord gives you the faultCode with a short description of the nature of the error that occurred. There is no standard list of faultCode:s to follow, so it is up to the API to decide them.

---

### Examples

**HTTP 404**

```
{
  "compositeFault": {
    "faults": [
      {
        "paramValues": [
          {
            "param": "cartid",
            "value": "STNRRV3A42QDAPCM"
          }  
        ],
        "explanationText": "ID not found",
        "faultCode": "SAO-001"
      }
    ]
  },
  "message": "ID not found"
}
```

**HTTP 400**

```
{
  "compositeFault": {
    "faults": [
      {
        "paramValues": [
          {
            "param": "cartid",
            "value": "STNRRV3A42QDAPCM"
          }  
        ],
        "explanationText": "Unable to produce label",
        "faultCode": "SAO-002"
      }
    ]
  },
  "message": "Unable to produce label"
}
```

