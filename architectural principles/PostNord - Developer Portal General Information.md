# General Information

### Here you will find general information you need to get started using PostNord APIs

## Quick Start Guide

PostNord provides a range of API�s to facilitate integration with your back end systems. Integrate PostNord web services directly into your website, customs applications or e-commerce platform. 

Our API are easy to use and customizable to meet your shipping needs.

1. Read the developer API documentation
2. For development
   - Sign up for a **Sandbox**
   - The **test API key** is provided to you in the registration email 
   - The test URL Host is: `atapi2.postnord.com`
3. For production
   - Sign up for  **Free/Premium plan**
   - The **production API key** is provided to you in the registration email 
   - The production URL Host is: `api2.postnord.com`

For help, contact api.suppport@postnord.com

## API Key

All of PostNord APIs requires the API key to be sent in the request as a query parameter `apikey` .

The API key is used to identify you as a consumer of APIs.

**Example**

`https:///{URL-host}/test?apikey=91f146137449a4068b3523a80xxx`

## HTTP Headers

Allows the client and the server to pass additional information with the request or the response. 

The table lists recommended HTTP headers, to be set by the client.

| Header                   | Example                           | Description                                                  |
| ------------------------ | --------------------------------- | ------------------------------------------------------------ |
| `Content-Type:`          | `application/json`                | The MIME type of the request body                            |
| `X-External-Request-ID:` | `904344e8cde5-f058ebd6-02f7f942e` | Unique transaction ID, used for transactional correlation between the client and PostNord (troubleshooting) |
| `Authorization:`         | `Bearer 1ijDG7gOOZ9Cj6VeH9tq`     | Authentication credentials, which may contain the Access-token from the Oauth2 login |

## **Authentication**

PostNord **OAuth2** implementation supports the standard **client credential** grant type.

- Each **secured API** has a set of **scopes** that defines the **permissions** to access their resources
  - They are listed in the  **API swagger definition** 
- The **client** must be **registered**, to be able to perform an OAuth2 login
  - A **successful login** creates an **Access token** to the client with the assigned scopes.
  - A client can choose to request all scopes or a subset of those defined  
- The **Access token** is **used** in the **calls** to PostNord **secured APIs**

### API Swagger definitions

They are published under the API docs section in the developer portal. 

- The secured APIs has a `securityDefinitions` in their swagger defining the required scopes
- Reference: [Swagger specification - OAuth](/https://swagger.io/docs/specification/authentication/oauth2/)

```json
"securityDefinitions": {
	"OAuth2": {
		"type": "oauth2",
		"tokenUrl": "https://pp-gate.ess.postnord.com/mga/sps/oauth/oauth20/token",
		"flow": "application",
		"scopes": {
			"https://api.postnord.com/scopes/shipment/declare/vat/write": "Modify the resource"
		}}}
```

### Client (application)

This represents your application that want to use the protected APIs. 

1. You need to register the client
2. Send the following information to api.support@postnord.com with the **subject: IAM**

| Client Data             | Description                                                  |
| ----------------------- | ------------------------------------------------------------ |
| API Key                 | The production key from the sign up                          |
| Client Application Name | The name of the client.                                      |
| Redirect_URI*           | After successful authorization, the authorization server will redirect the user back to the application with either an authorization code or access token in the URL. |
| Scopes                  | Provide a list of the scopes that the client needs access to, retrieved by looking into the APIs swagger specifications |

3. PostNord will supply you the following information to be used in the OAuth2 authorizations request.

| Client Data   | Description                                                  |
| ------------- | ------------------------------------------------------------ |
| client_id     | The `client_id` is a public identifier for the client        |
| client_secret | The `client_secret` is a secret known only to the client and authorization server |

### Authorization request

How to obtain an access-token for the client.

- Test Authorization server URL: ` https://pp-gate.ess.postnord.com/mga/sps/oauth/oauth20/token`
- Production Authorization server URL: `https://gate.ess.postnord.com/mga/sps/oauth/oauth20/token` 

#### OAuth - Client credential flow

##### Request an access-token without user interact.

```json
curl -X POST \
  https://pp-gate.ess.postnord.com/mga/sps/oauth/oauth20/token \
  -H 'Authorization: Basic ZGVjbGFyZXZhdDpjWjVMQlJUdWpFYWloajQxY0Uw==' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'grant_type=client_credentials
      &scope=all'
```

Use **HTTPS POST** to the authorization server

The **Authorization Header** contains **Basic** and the base64 encoded value of `client_id:client_secret`

- `Authorization: Basic ZGVjbGFyZXZhdDpjWjVMQlJUdWpFYWloajQxY0Uw==`

The **Content-Type Header** contains

- `application/x-www-form-urlencoded`

The **Body** contains

- `grant_type`: must be `client_credentials`
- `scope`: all returns the configured scope for the `client_id`


```json
curl -X POST \
  https://pp-gate.ess.postnord.com/mga/sps/oauth/oauth20/token \
  -H 'Authorization: Basic ZGVjbGFyZXZhdDpjWjVMQlJUdWpFYWloajQxY0Uw==' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'grant_type=client_credentials
      &scope=all'
```

##### Response message 

The **HTTP Headers** contains

- `Content-Type: application/json` Returned content type to the client
- `Cache-Control: no-store` Ensures that the clients doesn't  cache the request
- `Pragma: no-cache` Ensures that the clients doesn't cache the request

The **Body** contains

- `access_token`: The access token string as issued by the authorization server
- `sub`:  Identifies the principal that is the subject of the JWT
- `acr`: Authentication context reference
- `scope`:  Lists scopes attached to the token that were granted by the user
- `token_type`: The type of token this is, typically just the string �bearer�
- `expires_in`: The duration of seconds the access token is granted for


```json
HTTP/1.1 200 OK
Content-Type: application/json
Cache-Control: no-store
Pragma: no-cache

{
	"access_token": "o5Ptk3GUEgNV3izvuDl5",
	"sub": "dvat_client",
	"acr": "ActiveDirectory",
	"scope": "https:\/\/api.postnord.com\/scopes\/shipment\/declare\/vat\/write",
	"token_type": "bearer",
	"expires_in": 3599
}
```

##### PostNord secured API request

The **Authorization Header** must contain token_type **Bearer** with the **Access token** from OAuth login. 

* `Authorization: Bearer o5Ptk3GUEgNV3izvuDl5`

