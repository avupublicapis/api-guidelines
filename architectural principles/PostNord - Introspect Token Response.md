# PostNord - Introspect Token Response
The OAuth 2.0 Token Introspection extension defines a protocol that returns information about an access token, intended to be used by resource servers or other internal servers

| attribute        | example                                                    | description                                                  |
| ---------------- | ---------------------------------------------------------- | ------------------------------------------------------------ |
| sub              | erik.ekstrom@posten.se                                     | The ID used in the login by the end-user                     |
| acr              | ActiveDirectory                                            | Indicates how the subject was authenticated                  |
| scope            | https://api.postnord.com/scopes/shipment/declare/vat/write | Lists scopes attached to the token that were granted by the use |
| jws_access_token | eyJhbGciOi………                                              | JWT is the encoded reference of the access                   |
| active           | true                                                       | This is the value of whether or not the token is currently active |
| token_type       | bearer                                                     | The type of token this is, typically just the string “bearer” |
| exp              | 1504682396                                                 | The timestamp indicating when this token will expire         |
| iat              | 1504678803                                                 | The time the JWT was issued                                  |
| client_id        | introspection                                              | Will be the user performing the introspection (API GW)       |
| username         | erik.ekstrom@posten.se                                     | A human-readable identifier for the user who authorized this token |

