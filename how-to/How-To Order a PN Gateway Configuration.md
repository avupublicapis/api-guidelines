# How-To Order: PN Gateway Configuration

Short description of how to order configurations in the PN API Gateways. 

## API Information
---
### API Gov Team
- Slack user group: **@APIGOV** 
- Slack channel: **#api_management**
---
### PostNord Developer Portal

Used for communication with developers; includes static content, API documentation and terms of usage.

**Production: https://developer.postnord.com**  

**Sandbox: https://atdeveloper.postnord.com**  

> **Sign up** at the developer portal to retrieve an **apikey**
>
> A developer portal account is assigned the **role public per default** (views only public APIs).
>
> Request a different role (partner APIs, internal APIs) by contacting the API Gov Team.

---

### PostNord Gateways

**Primary: https://api2.postnord.com**  

> Entry point for external (Internet) clients to **RESTful APIs**

**Internal: http://internalapi.postnord.com**  

> Entry point for internal clients to all **RESTful** and **SOAP APIs**

**Sandbox/Acceptance: https://atapi2.postnord.com**

> API playground for developers

---
### Order Routine Procedure

1. Fill in the order form
2. Create a JIRA Task in: [Jira Project API Management](https://pncorp.atlassian.net/secure/RapidBoard.jspa?rapidView=215&projectKey=AM)
3. Contact the API Gov Team (with a link to the created JIRA Task)

#### Order form
The following information needs to be supplied in the JIRA Task.

| API                              | Description                                                  | Example                                                      |
| -------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Business Owner                   | Contact Information to the "product owner"                   | Firstname, Lastname, Phone, Email                            |
| Team                             | Responsible agile Team or AM object                          | Team Shipping APIs                                           |
| Team Slack                       | The slack channel used for the Team or AM object             | #api_management                                              |
| Swagger (WSDL)                   | Location to where the Swagger OR WSDL for the API can be found | https://app.swaggerhub.com/apis/postnord/shipping-v1-label/1.0.8 |
| API Located                      | Within or Outside PostNord Data Centre                       | Outside                                                      |
| Backend requests for **AT**      | Provide a valid backend request (non-harmful) & Health endpoint request. Preferable supply a Postman or Soap-UI project. | curl -X GET   https://at.execute-api.eu-west-1.amazonaws.com/test/v2/label/manage/health |
| Backend requests for **PROD**    | Provide a valid backend request (non-harmful) & Health endpoint request. Preferable supply a Postman or Soap-UI project. | curl -X GET   https://prod.execute-api.eu-west-1.amazonaws.com/test/v2/label/manage/health |
| Backend GW                       | Define Backend GW e.g. NGINX, Apache, AWS ELB/ALB/NLB, Kong, AWS Cloud Front, Server Name Indication (SNI), AWS API GW, ... | AWS ELB - AWS API GW                                         |
| Secured API                      | Using PostNord IAM or. The swagger must included a securityDefinitions | False                                                        |
| CORS                             | Enable CORS  for the API                                     | False                                                        |
| Others                           | Can be required HTTP Headers, X-API-KEY,...                  | n/a                                                          |
| Peak Volume/minute               | Initial peak volume first 6 month                            | 100 requests/min                                             |
| Daily Volume                     | Initial daily volume first 6 month                           | 2000 requests/min                                            |
| First client                     | If possible list the first client                            | NCP                                                          |
| **Requested Configuration Date** | Deadline of configuration. Does it has a dependency to a software deployment. | 2018-11-28 10:00                                             |

#### Public URL Naming 

The following section will be filled in by the API-Gov Team.

```
Public URL: 	/rest/shipment/v1/label/itemid
Backend URL:	/test/v2/label/itemid/57059830030683290/zpl

proxy_pass statement:			
				/rest/shipment/v1/label -> /test/v2/label
				/rest/shipment/v1/label/manage/health -> /test/v2/label/manage/health
```



