# API Performance Testing
------

[TOC]

------

## Revision history

| Date       | Version | Change      | Name          |
| ---------- | ------- | ----------- | ------------- |
| 2017-06-20 | 0.1     | First draft | Fredrik Bodin |


## Introduction

Performance testing is type of testing performed to evaluate the different performance attributes of the application like - responsiveness, stability, reliability etc. For determining these attributes, we have different types of performance testing techniques.

### Load testing

Load testing is a type of testing which involves evaluating the performance of the system under expected workload. A typical load test includes determining the response time, throughput, error rate etc. during the course of the load test.

> For a newly developed application with anticipated load of around 1000 concurrent user. We will create a load test script and configure it with 1000 virtual users and run it for say 1 hour duration. After the load test completion, we can analyze the test result to determine how the application will behave at the expected peak load.

### Stress testing

Stress testing is a type of performance testing where we evaluate application's performance at load much higher than the expected load. Another aspect of the stress testing is to determine the breakpoint of the application, the point at which the application fails to respond in correct manner.

> For an application with anticipated load of 1000 users we will run the test with for 1200 users and check if the application is robust enough to not crash.

### Endurance testing

Endurance testing is also known as 'Soak Testing'. It is done to determine if the system can sustain the continuous expected load for long durations. Issues like memory leakage are found with endurance testing.

> For an application like Income tax filing, the application is used for continuously for very long durations by different user. In these type of applications, the memory management is very critical. For application like these, we can run the test for 24 hours to 2 days duration and monitor the memory utilization during the whole test execution.

### Spike testing

In spike testing, we analyze the behavior of the system on suddenly increasing the number of users. It also involves checking if the application is able to recover after the sudden burst of users.

> For an e-commerce application running an advertisement campaign, the number of users can increase suddenly in a very short duration. Spike testing is done to analyze these type of scenarios.

### Volume testing

The volume testing is performed by feeding the application with high volume of data. The application can be tested with large amount of data inserted in the database or by providing a large file to the application for processing. Using volume testing, we can identify the bottleneck in the application with high volume of data.

> For a newly developed e-commerce application, we can perform volume testing by inserting millions of rows in the database and then carry out the performance test execution.

## Tools

There are several tools in the market, which preforms the above performance testing techniques. Below is a quick start for two of them.

### JMeter

JMeter is Java based, so it runs all the operating systems that are Java compliant. 

**Installation:**

- Make sure your machines has latest version of **Java (JVM) installed**.
- Download the appropriate JMeter binary from **Binaries** section of [Apache JMeter Official Website](http://jmeter.apache.org/download_jmeter.cgi).
- Extract the JMeter binary to a directory where we want JMeter to be installed.
- Now we can launch JMeter by using **jmeter.bat** or **jmeter.bat** file inside the bin folder.

An alternate way of installing JMeter on Mac is using **Homebrew**

```
brew install jmeter
```

#### RestFul API usage

A Test Plan is a logical container that contains different operations for performing a performance test. The different operation in a performance test are carried out by different elements of the Test Plan.

1. First of all we need to add an **HTTP Request Sampler** inside a Thread group: 
    - Test Plan -> Thread Group -> Sampler -> HTTP Request
2. Enter the API information into the **HTTP Request section**
    - Server Name or IP 	    - DNS name of the server (e.g. yourapi.postnord.com)
    - Implementation 		- Select HttpClient4 or Java
    - Protocol[http] 		- Depending on type of protocol, select HTTP or HTTPS
    - Method 				- HTTP verb (post, get, put, delete, etc.)
    - Path 					- Application path after the DNS name (http://yourapi.postnord.com/posts' the Path will be "posts"
    - Parameters/Post Body	- Add parameters or request body here (request body of the API in "Post body" section)
3. User also need to add **HTTP Header Manager** as child to the **HTTP Request sampler**
    - Click on Add button on HTTP Header Manager and add for example "Content-Type" under Name and "application/json" under Value.
4. Next, can you add a **Response Assertion** to the test plan in order to mark the test case as pass or fail based on the response fetched e.g. verify the response data. 
    - HTTP Request -> ADD -> Assertion -> Response Assertion; add the expected response pattern to Test section of Response Assertion 
5. Add the listener 'View Result Tree' to conclude the test plan.
6. Run the JMeter script by Ctrl+r
    - Verify the result in the "View Result Tree"
7. Tune the "Number of Thread" and "Loop Count" in the Thread Group
8. [JMeter manual](http://jmeter.apache.org/usermanual/component_reference.html#HTTP_Request)

Download a [JMeter Performance test template](https://bitbucket.org/avupublicapis/api-guidelines/src/b0a77f7653faaa9791d74bb00de0e27b9f183111/examples/JMeter%20Performance%20Test%20API%20template.jmx?at=master)

------

### Apache Bench

**AB**( Apache Bench) is a command line utility to measure performance of any web server. Its originally designed to test the Apache **HTTP** server. **AB** tool will shows you how many request per second your API can handle.

- Its is recommend to start from small concurrent repetition and increase the numbers until your result fails or the load reaches your defined limit.

**Installation:** 

*In Redhat or Centos:*

Install httpd-tools to get AB tool.

```
$ yum install httpd-tools
```

#### **RestFul API usage**

```
$ ab -n 10 -c 10 http://yourapi.postnord.com/rest/shipment/v1/tracking/{id}
```

**Parameter Description:**

- -n 10: ab will send 10 number of requests to server yourapi.postnord.com
- -c 10 : 10 multiple requests to perform at a time. Default is one request at a time.
- -e output.csv  : writes a comma separated value (CSV) file containing percentage (from 1% to 100%) the time (in milliseconds) it took to serve that percentage of the requests
- [All available parameters](https://httpd.apache.org/docs/2.4/programs/ab.html)

**POST JSON body usages:**

```
# post_body.txt contains the json you want to post
# -p means to POST it
# -H adds an Auth header (could be Basic or Token)
# -T sets the Content-Type
# -c is concurrent clients
# -n is the number of requests to run in the test
# -I Don't report errors if the length of the responses is not constant. Available in 2.4.7 and later

$ab -p post_body.txt -T application/json -H 'Authorization: Token abcd1234' -I -c 10 -n 2000 http://example.com/rest/locations/v1
```

**Output:**

```
ab -n 10 -c 10 http://localhost/index.php
This is ApacheBench, Version 2.3 <$Revision: 655654 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done

Server Software:        Apache
Server Hostname:        localhost
Server Port:            80
Document Path:          /
Document Length:        38879 bytes
Concurrency Level:      10
Time taken for tests:   6.401 seconds
Complete requests:      10
Failed requests:        0
Write errors:           0
Total transferred:      392381 bytes
HTML transferred:       388790 bytes
Requests per second:    1.56 [#/sec] (mean)
Time per request:       6400.818 [ms] (mean)
Time per request:       640.082 [ms] (mean, across all concurrent requests)
Transfer rate:          59.86 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:      365  366   0.7    366     367
Processing:  2214 3170 1171.5   3006    6020
Waiting:     1634 2424 775.4   2430    3924
Total:       2580 3537 1171.6   3371    6387

Percentage of the requests served within a certain time (ms)
  50%   3371
  66%   3580
  75%   3603
  80%   4516
  90%   6387
  95%   6387
  98%   6387
  99%   6387
 100%   6387 (longest request)
```