# How-To Onboarding an API
------
 
[TOC]

## Revision history

| Date       | Version | Change      | Name          |
| ---------- | ------- | ----------- | ------------- |
| 2017-06-21 | 0.1     | First draft | Fredrik Bodin |

## Introduction

This guide contains the procedure and instructions to Onboard an API for a project or developer. At PostNord do we classify APIs into 3 definitions:

- **Internal - internalapi.pninfrasrv.net**: APIs that are accessible only within the security permitter of the organization by systems built and managed by employees of the organization. Ideally not accessible across the Public Internet.
- **Private - api3.postnord.com**: APIs that are accessible on an “invite only” basis to a limited set of third parties or software which is communicating across the public Internet – normally close partners (secured with a secret token or basic auth). This puts mobile backend APIs into this bucket since although the code may be written in house, access is across the public Internet.
- **Public - api2.postnord.com**: APIs that are accessible to any party that wishes to sign up (an **apikey** is required). While there may be a vetting process, it is primarily to filter out fraudulent use or competitors – for all others it is general access.

> Note: That the acceptance environment has a prefix of **at** on the gateway DNS names; atapi2.postnord.com, atapi3.postnord.com & atinternalapi.pninfraserv.net

## Build your API

These are the main steps to preform to build and make an API available.

**Step 1:** Start-up meeting with RDT API Team (api.support@postnord.com)

**Step 2:** Read and design the API after the following **guidelines**; [PostNord RestFul API Guidelines](https://bitbucket.org/avupublicapis/api-guidelines/src/c1d063f8890c40e585c03d464dc4560a3464f27e/Postnord%20-%20RestFul%20API%20Guidelines.md?at=master&fileviewer=file-view-default) 

**Step 3:** **Swagger first**, manually create a swagger 2.0 definition for the API using the [PostNord Swagger 2.0 template.json](https://bitbucket.org/avupublicapis/api-guidelines/src/b0a77f7653faaa9791d74bb00de0e27b9f183111/examples/Postnord%20-%20Swagger%202.0%20template.json?at=master)   

**Step 4:** **Develop** the API according to the PostNord guidelines 

**Step 5:** Functional and performance **testing** of the API. 

**Step 6:** Update the **API Repository documentation** for the API. 

**Step 7:** Order the specific **gateway configuration** 

**Step 8:** API **Developer portal configuration**

**Step 9:** Order **surveillance** for the API.

**Step 10:**  Decided **GO-live date**

------

## Start-up with the RDT API Team

This meeting introduces and explains the procedure to expose and develop an API at PostNord, to maintain a high quality across APIs.  It also works as a facilitator, to get the knowledge about who is responsible for what and how things are done. 

###  Deliverables

- Identify special requirements, that needs to be addressed e.g. security, legal, billing, etc
- API documentation placeholder for the API
	- Order access to PostNord Confluence from itpso@postnord.com
	- Create a placeholder in the [Confluence API Repository](https://pncorp.atlassian.net/wiki/display/AM/API+Repository)
- Decided in which gateways the API should be published: 
	- **public | private | internal**
- Agree upon the **API URL**, must conform to the PostNord convention
	- e.g: **/rest/shipment/v1/collectcode**
- Decided the API Gateway configuration options
	- Track usage: **true/false**
	- Application plan: **Basic plan**
	- System_name: **shipment-v1-collectcode** (based from the API URL)
	- ...
- Credentials to the @AT developer portal
	- URL, User, Password


## API Developer portal configuration

This is a run through how to make the API swagger available and visible in the developer portal. There are two developer portals at PostNord;

- Production: 	**developer.postnord.com**
- Acceptance:   **atdeveloper.postnord.com**

> The import into production, is preformed by the AM supplier. 

### Modify Swagger

> Explains the necessary modifications  required on the APIs swagger definition; so it is shown in a good way and is aligned with the developer portal import requirements. 

- Generate or create the Swagger 2.0 definition from the API in JSON format (the only supported import format)

- Modify the Swagger according to the following rules

  - **General changes**

    - Add the parameter **apikey** according to the following definition. 

    ```
    		{
    		 "name": "apikey",
    		 "description": "The unique consumer (client) identifier 32 characters",
    		 "required": true,
    		 "type": "string",
    		 "in": "query",
    		 "threescale_name": "user_keys"
    		}
    ```

    > The `"threescale_name": "user_keys"` enables a lookup against the signed in user accounts apikey and is not a valid Swagger attribute.

    - Remove the `health endpoint`, should not be public visible.
    - Align the swagger with the [PostNord Swagger template](https://bitbucket.org/avupublicapis/api-guidelines/src/c1d063f8890c40e585c03d464dc4560a3464f27e/Postnord%20-%20Swagger%202.0%20template.json?at=master&fileviewer=file-view-default)
      - Use the defined HTTP status texts from the swagger template.
      - Use the defined common PostNord fault object.
      - Use predefined Information Objects

  - **Swagger 2.0 specific changes**

    - Remove the **info object** due to developer portal limitations
    - Set the **host** attribute to the used Gateway:  `"host": "atapi2.postnord.com"`
    - Set the **basepath** from the APIs URL : `"basePath": "/rest/shipment/v1"    `
    - Set the **paths** from the APIs URL : `"paths": { "collectcode":....}    `
    - The swagger security definitions can't be used due to developer portal limitations

  - **Swagger 1.2 specific changes**

    - **Should not be used!**
    - Set the **version** of the API (major.minor.revision):  `"apiVersion": "1.0.0"`
    - Set the **basepath** from the APIs URL : `"basePath": "atapi2.postnord.com"` 
    - Set the **resourcePath** from the APIs URL : `"resourcePath": "/rest/shipment/v1/collectcode"    `
    - Set the **path** from the decided URL : `"path": "/rest/shipment/v1/collectcode"    `
    - Set these specific attributes for each **operation**

		```
		"method": "PUT",
		"summary": "Validate digital id",
		"notes": "Validate digital id",
		"type": "void",
		"nickname": "validateDigitalId",
		```


### Import Swagger 

> Describes the procedure to import a swagger for a new API.

- Login into the specific developer portal
	- @AT enviroment: https://postnord-test-admin.3scale.net/admin/api_docs/services 
	- @AT user/password: **contact api.support@postnord.com**
  
- Open **ActiveDocs administration page**

![ActiveDocs](https://bytebucket.org/avupublicapis/api-guidelines/raw/1f5b0e6dde8e366f481b0cf51f3e1009233a8b9a/images/3scale%20activedocs.png)

------

- **Create a New Service Spec**
	- Name_*:  **(decided in the "start-up" and based from the API URL, e.g shipment-v1-collectcode )**
	- System name_*:  **(decided in the "start-up" and based from the API URL, e.g shipment-v1-collectcode)**
	- Publish?: **YES**
	- Description: **(add well written description, should already exists in the swagger most probably)**
	- API JSON Spec: **paste the Swagger defintion here** 
	- Skip swagger validations: **YES** (the validations errors are not always solvable)

![3scale activedocs new service](https://bytebucket.org/avupublicapis/api-guidelines/raw/1f5b0e6dde8e366f481b0cf51f3e1009233a8b9a/images/3scale%20activedocs%20new%20service.png)

------

- **Preview Service Spec**
	- Verify the swagger
	- Try it out (requires the API to be configured in the API gateway)
> Note: Only Public API should be visible, otherwise hide them

![3scale activedocs preview service spec](https://bytebucket.org/avupublicapis/api-guidelines/raw/1f5b0e6dde8e366f481b0cf51f3e1009233a8b9a/images/3scale%20activedocs%20preview%20service%20spec.png)

------

### Visualize Swagger 

> The CMS pages needs to be modified, so the new API is exposed on the developer portal website. This describes the procedure to accomplish that. 

- Open the following [URL](https://postnord-test-admin.3scale.net/p/admin/cms)

![3scale cms visualize new service](https://bytebucket.org/avupublicapis/api-guidelines/raw/1f5b0e6dde8e366f481b0cf51f3e1009233a8b9a/images/3scale%20cms%20visualize%20new%20service.png)

------

- For **Swagger 2.0** definitions
	- Are handled in: **Documentation2** 
	- Add the APIs decided **System name_* ** into the following sections in the page.

	```
	{% active_docs version: "2.0" services: "trackandtrace-swagger-2, location-v1-mailbox, shipmentinstruction-v5-modification,order-v1-pickup" %}

	var myServices = "address-v1-surcharge,address-v1-addressdictionary,shipmentinstruction-v1-flex,customer-v1-options,customer-v1-dropoff,shipmentinstruction-v5-modification,order-v1-pickup";
	```

- For **Swagger 1.2** definitions
	- Are handled in: **Documentation** 
	- Add the APIs decided **System name_* ** into the following sections in the page.

	```
	{% active_docs version: "1.2" services: "trackandtrace,shipment-v2-trackandtrace,transittime,
	transittime-v1-servicecoverage,servicepoint,servicepointinformation,links-v1-tracking,customer-v1-timeslot,transittime-v2-list-additionalservices" %}
	```

- **Publish** the change.
- **Verify** everything in the [developer portal](https://atdeveloper.postnord.com)
- Sign in and **make a successful request!**

### Check-in Swagger to Bitbucket

- Check-in the imported swagger to the following [bitbucket repo](https://bitbucket.org/avupublicapis/api-swagger-at/docs)