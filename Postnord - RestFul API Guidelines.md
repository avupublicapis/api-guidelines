# Postnord - RestFul API Guidelines
------



[TOC]

------

## Revision history

| Date       | Version | Change      | Name          |
| ---------- | ------- | ----------- | ------------- |
| 2017-06-07 | 0.1     | First draft | Fredrik Bodin |


## Introduction

The “RESTful API guidelines” aims to define a standard to successfully establish a high quality on the published APIs. Consistent design across an organization’s APIs ensures the smoothest possible experience for developers using the APIs. These guidelines should also help Teams to iterate on the API’s design quickly, and make the implementation faster. There should be no misconceptions on how the API should work with regards to design; then developers, devOps or testers, can work better on creating a functioning API. 

> Teams are responsible to fulfill these guidelines during API development, and are encouraged to contribute to guideline evolution.

A well-designed API should address the following:

- **Business focus:** APIs should be designed with a specific audience in mind, providing a solution to a problem they are having. Make APIs reflect how they will be used, and experienced, over where the API came from and the resource it was derived.
- **Simplicity:** Consider simplicity at ever turn when designing APIs, providing the smallest possible unit of value you possibly can simplicity goes a long way.
- **Consistency:** Employ consistent approaches to all aspects of API design, providing a familiar approach across all APIs published.
- **Easy to use:**  While APIs are for computers, they should be easy to read by humans, making documentation more accessible.
- **Hard to misuse:** When APIs do one thing, and does it well, you reduce the opportunity for misuse, and people putting them to work in unintended ways.
- **Evolvability:** The API should be able to evolve and add (or remove) functionality independently from client applications. Existing client applications should be able to continue to operate unmodified as the features provided by the API changes. All functionality should also be discoverable, so that client applications can fully utilize it.
- **Availability:** It is very important that the API is up and running all the time, this is one of the keystones to successful customer experience. To accomplish this must the whole technical setup be considered, not only the API codebase. The goal is also to build APIs, so that a failure never affects the company way of doing business. 
- **Performance:** The user experience of the API should always be positive, when it comes to the performance aspect. The API should be built to handle e.g. high volume, auto scale, fast response time, limited degradation of performance metrics during peak load. It is also important to benchmark the API, so the performance thresholds are known.

## Definitions

The following are the most important terms related to REST APIs

- **Resource:** This is the fundamental concept in any RESTful API. A resource is an object or representation of something, which has some associated data with it and there can be set of HTTP methods to operate on it. 
- **Collections:** Resources can be grouped into *collections*. Each collection is homogeneous so that it contains only one type of resource, and unordered. Resources can also exist outside any collection. In this case, we refer to these resources as *singleton resources*. Collections are themselves resources as well.
- **URL** (Uniform **Resource** Locator) is a path through which a **resource **(nouns) can be located and some actions (HTTP methods) can be performed on it.  The URL is composed of different parts, some mandatory and others are optional e.g `http://www.example.com:80/path/to/myfile.html?key1=value1&key2=value2#SomewhereInTheDocument ` 

## Principles

This is a list of common design principles that applies to building APIs

- **API Blueprint:** Always create an API definition using the latest swagger specification. Online tool: http://editor.swagger.io
- **Encoding in UTF-8:** Make sure you use UTF-8 encoding for your API responses, supporting proper encoding.
- **All objects/attributes are common to all APIs:** Must conform to the PostNord Information Object Model (TBD)
- **Time zones:** Accept any time zone in requests, due to that consumers are global.
- **Timestamps:** Are returned in as defined by [RFC3339](http://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14) `UTC with zero offset: YYYY-MM-DDThh:mm:ssZ 2016-04-12T23:20:50.52Z`
- **Caching & ETags:** Should be applied, if possible to increase performance and off-load APIs.
- **CORS:** Enable CORS for API endpoints, to allow cross-domain requests. For security reasons, browsers restrict cross-origin HTTP requests initiated from within scripts. 
- **Request-ID:** Use Request-Ids to providing added details for logging, auditing, and reporting on API usage. `X-Request-ID & X-External-Request-ID`
- **Rate limits:** All public APIs has an assigned rate limit associated with the registered developer account.
- **Media Types:** All APIs should support responses in both; application/json and application/xml media types.
- **Compression:** If applicable use compression e.g. gzip to improve performance.

## HTTP Headers

Are components of the header section of request and response messages in the Hypertext Transfer Protocol (HTTP). They define the operating parameters of an HTTP transaction. The table lists common and non-standard headers, of importance and understanding.

| Field name               | Description                              | Example                                  |
| ------------------------ | ---------------------------------------- | ---------------------------------------- |
| Accept                   | Content-Types that are acceptable for the response. | Accept: application/json                 |
| Accept-Charset           | Character sets that are acceptable       | Accept-Charset: utf-8                    |
| Accept-Language          | List of acceptable human languages for response | Accept-Language: en-US                   |
| Cache-Control (request)  | Used to specify directives that must be obeyed by all caching mechanisms along the request-response chain. General example. | Cache-Control: private, max-age={seconds}, must-revalidate |
| Cache-Control (response) | Tells all caching mechanisms from server to client whether they may cache this object. It is measured in seconds | Cache-Control: max-age=3600              |
| Content-Type             | The MIME type of the body of the request (used with POST and PUT requests) | Content-Type: application/x-www-form-urlencoded |
| ETag                     | An identifier for a specific version of a resource, often a message digest | ETag: "737060cd8c284d8af7ad3082f209582d" |
| If-Match                 | Only perform the action if the client supplied entity matches the same entity on the server | If-Match: "737060cd8c284d8af7ad3082f209582d" |
| If-None-Match            | Allows a 304 Not Modified to be returned if content is unchanged | If-None-Match: "737060cd8c284d8af7ad3082f209582d" |
| If-Range                 | If the entity is unchanged, send me the part(s) that I am missing; otherwise, send me the entire new entity | If-Range: "737060cd8c284d8af7ad3082f209582d" |
| Allow                    | Valid methods for a specified resource. To be used for a 405 Method not allowed | Allow: GET, HEAD                         |
| Retry-After              | If an entity is temporarily unavailable, this instructs the client to try again later. Value could be a specified period of time (in seconds) or a HTTP-date | Retry-After: 120                         |
| X-Request-ID             | Unique transaction ID generated by the PostNord permitter server (i.e. api2.postnord.com) | X-Request-ID: f058ebd6-02f7-4d3f-942e-904344e8cde5 |
| X-External-Request-ID    | Represents the incoming unique transaction ID from the client. The client can use it for acknowledgment, report, and response messages to reference the original message | X-External-Request-ID: f058ebd6-02f7-4d3f-942e-904344e8c645 |
| Age                      | The age the object has been in a proxy cache in seconds | Age: 12                                  |
| X-Varnish                | Allows you to find the correct log-entries. For a cache hit, X-Varnish will contain both the ID of the current request and the ID of the request that populated the cache. | X-Varnish: 1886109724 1886107902         |
| X-Forwarded-For          | Standard for identifying the originating IP address of a client connecting to a web server through an HTTP proxy or load balancer | X-Forwarded-For: 129.78.138.66, 129.78.64.103 |
| X-Access-Token           | Used to pass the Access token from OAuth 2.0 or OPENID connect authorization | X-Access-Token: T8TGH9IOEc2q7kpuvCLjkZnGDKAV0uSVSj9r8czi |
| Authorization            | Authentication credentials for HTTP authentication. Which can be Basic Auth (Basic) and JWT from Oauth2.0 (Bearer). | Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ== |
| Expires                  | Gives the date/time after which the response is considered stale | Expires: Thu, 01 Dec 1994 16:00:00 GMT   |

## Request

HTTP is a client-server protocol: requests are sent by one entity, the user-agent (or a proxy on behalf of it). Each individual request is sent to a server, which will handle it and provide an answer, called the *response*. The server-side RESTful API has an URL that defines its resources, which is requested by the entity.

### URL convention

The PostNord structure has the intention to enforce a common way of naming APIs. This is described below using the Swagger specification format.

```
scheme://host/basepath/paths
```

These Public URL conforms to the PostNord naming structure convention, explained below using the swagger defintion build up.

```
https://api2.postnord.com/rest/customer/v1/identity/101
https://api2.postnord.com/rest/transport/v2/transittime
https://api2.postnord.com/rest/shipment/v5/tracking/id/86506224593SE
https://api2.postnord.com/rest/location/v1/address/search
https://api2.postnord.com/rest/retail/v1/agent/SE/items/121
https://api2.postnord.com/rest/transport/v1/order/ruralpostman/delivery
```

#### Scheme

Schemes are the transfer protocols used by the API 

```
http	- plain HTTP
https 	- HTTP over SSL
ws		- Websocket
wss		- Websocket over SSL
```

#### Host

Host is the domain name of the host that serves the API. It may include the port number if different from the scheme’s default port (80 for HTTP and 443 for HTTPS). 

- Note that this must be the host only, without http(s):// or sub-paths.

```
#Valid hosts				#Scope		#Comment
---------------------------------------------------------------------------------------------------------------------------------
api2.postnord.com			Public		APIs that are accessible to any party that wishes to sign up.
api3.postnord.com			Private 	APIs that are accessible on an “invite only” basis to a limited set of third partners. 
internalapi.pninfrasrv.net	Internal	APIs that are accessible only within the security permitter of the organization.

Note: The prefix 'at' is used on the valid hosts to access the corresponding acceptance environment (e.g. atapi2.postnord.com).
```

#### BasePath

The basePath is the URL prefix for all API paths.

- All BasePath are relative to: **Host**

It must conform to the following common PostNord structure:

```
/tag/context/vN/ 
```

##### tag

Is right now used to define the API technology `/rest` . But can in the future also be used to route incoming traffic to a specific target using a character abbreviation `/abc` , this enables us to have a greater configuration flexibility.

```
/rest - restFul services
/web  - web pages (html)
/soap - soap services
/auth - authentication
/adm  - healthchecks
```

##### context 

Groups the APIs according to their main information/business concept that is subject to the provided functionality. This also works as an hierarchy level in the developer portal, to make it easier presenting the APIs business domain. New concepts must be approved by the api.support@postnord.com

```
#Context			#Description  
---------------------------------------------------------------------------------------------------------------------------------
customer			APIs related to customers, recipients, TA-suppliers, notification 
shipment  			APIs related to information and actions on shipments; tracking, collectcode, flex, modify   
location  			APIs related to information about servicepoints, address, mailboxes, production points, collect-in-stores  
transport  			APIs related about EDI, labels, service coverage, timeslot , pickups, transittime  
retail  			APIs related to retail domain; Retail agents, Collect In-store
system  			APIs related to system domain; SAP, Salesforce, CINT
masterdata 			APIs related to master data domain; Customer, Locations, Address
```

##### vN -versioning

Defines the major version of the API  `e.g. /v1 ` , the API must be able to evolve overtime to minimize the need for new versions. 
The following changes can be introduced to the API that do not constitute a breaking change:

- A new resource or API endpoint
- A new optional parameter
- A change to a non-public API endpoint
- A new optional key in the JSON POST body
- A new key returned in the JSON response body

Conversely, a breaking change included anything that could break the integration such as:

- A new required parameter
- A new required key in POST bodies
- Removal of an existing endpoint
- Removal of an existing endpoint request method
- A materially different internal behavior of an API call — such as a change to the default behavior.



#### Paths

Are endpoints (collections/resources) that your API exposes; URI references for resources should consistently use the same path components to refer to resources.

- All Paths are relative to: **BasePath**
- The **2-letter country code** can be used in the endpoint to separate country specific resources `e.g. /feedback/SE/summary`.
- Monitor endpoints should be placed under /manage  `e.g. /feedback/manage/health`

##### Endpoint convention

```
/{collections}/{resource}/{resource-id}/{sub-resource}/{sub-resource-id}
```

> The structure differs depending on requirements 


### HTTP methods

The HTTP methods defines the operations used to manipulate these paths.

```
--- Common HTTP verbs
GET		Retrieve a copy of the resource at the specified URI. The body of the response message contains the details of the requested
		resource.
POST	Create a new resource at the specified URI. The body of the request message provides the details of the new resource. Note that
		POST can also be used to trigger operations that don't actually create resources.
PUT		Replace or update the resource at the specified URI. The body of the request message specifies the resource to be modified and the
		values to be applied. Note that PUT requests are idempotent whereas POST requests are not.
DELETE	Remove the resource at the specified URI.

--- Less common HTTP verbs
PATCH	Used to request selective updates to a resource.
HEAD 	Used to request a description of a resource.
OPTIONS Used to describe the communication options for the target resource. Is also used in CORS; to determine whether requests to other
        domains are safe to send (preflights requests)
TRACE 	Allows a client to request information that it can use for testing and diagnostics purposes.
```

### URI Endpoints

The summary table shows the recommended endpoints to be used with the corresponding effect for the basic HTTP methods.

| **Collection/Resource** | **POST**                                 | **GET**                                  | **PUT**                                  | **DELETE**                               |
| ----------------------- | ---------------------------------------- | ---------------------------------------- | ---------------------------------------- | ---------------------------------------- |
| */users*                | *Create a new user with the data in the body* | *Retrieve all users*  | *Bulk update of users (if implemented)*  | *Remove all users*                       |
| */users/1*              | *Error*                                  | *Retrieve the details for user 1*        | *Update the details of user 1 if it exists, otherwise return an error* | *Remove user 1*                          |
| */users/1/bookings*     | *Create a new booking for user 1*        | *Retrieve all bookings for user 1*       | *Bulk update of bookings for user 1 (if implemented)* | *Remove all bookings for user 1 (if implemented)* |
| */users/manage/health*  | *Error*                                  | *Retrieve API health status*             | *Error*                                  | *Error*                                  |
| */users/manage/metrics* | *Error*                                  | *Retrieve API metrics*                   | *Error*                                  | *Error*                                  |
| */users/api-docs*       | *Error*                                  | *Retrieve API Swagger definition*        | *Error*                                  | *Error*                                  |

### Parameters

Operations can have parameters that can be passed via URL path (`/users/{userId}`), query string (`/users?role=admin`), headers (`X-CustomHeader: Value`) and  `request body`. The recommendation is to use URL path or the request body approach, it is important to define the parameter type, format, whether they are required or optional, and other details (e.g. default values).

```
--- URL path 
/users/{id}
/organizations/{orgId}/members/{memberId}

--- Request body 
{"eventList": {
   "returns":    [
      {
         "itemId": "00000000045SE",
         "returnReason": "senderdemand",
         "missing": false,
         "timeStamp": "Tue May 09 2017 13:06:08 GMT+0200 (Västeuropa, sommartid)"
      }
   ]
}}

Note: The API client needs to provide appropriate parameter values when making an API call
```

#### Field name convention

Follows the camelCase convention for the field name convention; the name should also be shelf explained for a humans.

## Response 

The following considerations should be applied to the API response design, to deliver quality and enforce consistency. 

- Responses *should* include a `Last-Modified`. 
- Default content-type *should* be `application/json`
- All responses must include an `ETag` header, to support caching. It should be based on a hash of the response payload, not on timestamps.

### HTTP Status codes

The HTTP status codes should be used in a consistent way across all API operations. This section provides a non-exhaustive list of HTTP status codes that Postnord APIs attempts to return for every request. For more information: https://httpstatuses.com 

| Code | Reason                | Description                              |
| :--- | :-------------------- | :--------------------------------------- |
| 200  | OK                    | The request has succeeded                |
| 201  | Created               | Successfully created                     |
| 204  | No Content            | The server has successfully fulfilled the request to the target resource |
| 304  | Not Modified          | A GET or HEAD request has been received and would have resulted in a 200 OK response. Due to that the client already has a valid representation of the resource |
| 400  | Bad Request           | The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing) |
| 401  | Unauthorized          | API-key or authentication token is missing/invalid |
| 403  | Forbidden             | The server understood the request but refuses to authorize it |
| 404  | Not Found             | The origin server did not find a current representation for the target resource or is not willing to disclose that one exists |
| 405  | Method Not Allowed    | The method received in the request-line is known by the origin server but not supported by the target resource e.g. POST, GET, OPTIONS, PUT |
| 406  | Not Acceptable        | Indicates that the server cannot produce a response matching the list of acceptable values defined in the request's proactive content negotiation headers, and that the server is unwilling to supply a default representation |
| 429  | Too Many Requests     | The user has sent too many requests in a given amount of time ("rate limiting") |
| 500  | Internal Server Error | The server encountered an unexpected condition that prevented it from fulfilling the request |
| 502  | Not Implemented       | The server does not support the functionality required to fulfill the request |
| 503  | Service Unavailable   | The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay |
| 504  | Gateway Timeout       | The server, while acting as a gateway or proxy, did not receive a timely response from an upstream server it needed to access in order to complete the request. |

### HTTP Status Response Model
#### Client error responses (400 - 499)
##### Expected response from IAM, URL-https://account.postnord.com/oauth2/token
- 	Login failure
```
{
    "code": 401,
    "error": "invalid_request",
    "error_description": "The client_id is required for basic authentication."
}
```
<br />
- 	The access_token has expired
```
{
    "error": {
        "status_code": 401,
        "status": "Unauthorized",
        "description": "https://httpstatuses.com/401"
    }
}
```
<br />
- 	The access_token not available in request header
```
{
    "error": {
        "status_code": 403,
        "status": "Unauthorized",
        "description": "https://httpstatuses.com/403"
    }
}
```
##### Expected response from Gateway
-	 The APIKey is invalid or not available in request parameters 
```
{
    "error": {
        "status_code": 403,
        "status": "Forbidden",
        "description": "https://httpstatuses.com/403"
    }
}
```
<br />
-	 404 Not found
```
Not found
```
<br />
-	 429 Bapkey limit reached (Only incase you have integrated bapkey and passing in web)
```
{
    "error": {
        "status_code": 429,
        "status": "Too Many Request",
        "description": "https://httpstatuses.com/429"
    }
}
```
<br />
-	 429 Apikey limit reached
```
{
    "error": {
        "status_code": 429,
        "status": "Apikey limit reached",
        "description": "https://httpstatuses.com/429"
    }
}
```
<br />
-	 429 Too Many Request
```
{
    "error": {
        "status_code": 429,
        "status": "Too Many Request",
        "description": "Blocked by postnord, classified as Denial of Service "
    }
} 
```
#### Server error responses (500 – 599)
- 	502 Bad Gateway
```
{
    "error": {
        "status_code": 502,
        "status": "Bad Gateway",
        "description": "https://httpstatuses.com/502"
    }
}
```
<br />
- 	503 Service Temporarily Unavailable
```
{
    "error": {
        "status_code": 503,
        "status": "Service Temporarily Unavailable",
        "description": "https://httpstatuses.com/503"
    }
}
```
<br />
- 	504 Gateway Timeout
```
{
    "error": {
        "status_code": 504,
        "status": "Gateway Timeout",
        "description": "https://httpstatuses.com/504"
    }
}
```
	
#### Common fault object

```json
{  "compositeFault": {
    "faults": [
      {
        "paramValues": [
          {
            "param": "string",
            "value": "string"
          }
        ],
        "explanationText": "string",
        "faultCode": "string"
      }
    ]
  },
  "message": "string" }
```

> Do not return a **param** and **value** that are classified as a security risk.
>
> The Postnord Swagger template includes the definition of the common fault object

#### Response error principles

There are several ways to implement, how an API should respond to different error scenarios. 

##### Common

*Never return "500 - Internal Server Error" intentionally*

```
The general catch-all error when the server-side throws an exception
```

##### POST

*Create new resource*

```
"201 Created" + response body on resource creation (shall also include server generated values)
```

##### GET

*List a collection*

```
If the collection is empty (0 items in response), "404 Not Found" is not appropriate. 
The items array should just be empty, and collection metadata fields provided (e.g. "total_count": 0). 
Invalid query parameter values can result in "400 Bad Request" 
Otherwise "200 OK" is utilized for a successful response.
```

*Single resource*

```
If the provided resource identifier is not found, responds "404 Not Found" HTTP status
Otherwise, "200 OK" HTTP status should be utilized when data is found.
```

##### PUT/PATCH/DELETE

*Single resource*

```
Any failed request validation responds "400 Bad Request" + provide a specific error in the common fault object 
If clients attempt to modify read-only fields, this is also a "400 Bad Request"
If there are business rules, provide a specific error in common fault object for that validation.
If the resource doesn't exist return "404 Not Found"
After successful update, PUT operations should respond with "204 No Content" status, with no response body.
```

### Internationalisation (i18n)

An API *may* provide internationalised representations of entities. The client *may* specify their desired locale using the `Accept-Language` header as per [RFC 2616](http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.4) or a specific locale request attribute. If representation is localised, the API *should* include the `Content-Language` header in the response. 

> If the locale requested is not available, the API *should* respond with its default locale setting. 

## Dataset actions

Exposing a collection of resources, can lead to applications fetching large amounts of data when only a subset of the information is required. This generates traffic that can impact performance, scalability and the responsiveness of client applications requesting the data. All of these actions below are simply the query on one dataset, there will be no new set of APIs to handle these actions. 

### Metadata fields

Include metadata fields in the API, that can help clients to build dataset filter functionality easier. 

- Pages of results should be referred to consistently by the query parameters `page` and `page_size`, where `page_size` refers to the amount of results per request, and `page` refers to the requested page.
- Responses should include `total_count` and `total_pages` whenever possible, where `total_count` indicates the total items in the requested collection, and `total_pages` is the number of pages (from `total_count`/`page_size`).
- Hypermedia links should be provided with rels of `next`, `previous`, `first`, `last` wherever appropriate.
- Time selections follows the convention; `start_time` or `{property_name}_after`, `end_time` or `{property_name}_before` query parameters should be provided if time selection is needed.
- Sorting uses; `sort_by` and `sort_order` can be provided to allow for collection results to be sorted. `sort_by` should be a field in the individual resources, and `sort_order` should be `asc` or `desc`.

### Sorting

In case, the client wants to get the sorted list of items, the `GET /items` endpoint should accept multiple sort parameter in the query e.g `GET /items?sortBy=dateAsc` would sort the items by its date in ascending order.

### Filtering 

For filtering the dataset, we can pass various options through query parameter. E.g `GET /items?status=informed&country=SE` would filter the items list data with the item status of "informed" and where the country is Sweden.

### Searching 

When searching the consignor name in items list the API endpoint should be `GET /items?search=Mckinsey`

### Paging 

When the dataset is too large, we divide the data set into smaller chunks, which helps in improving the performance and is easier to handle the response. Eg. `GET /items?page=23` means get the list of companies on 23rd page.

### Slice

Add `start` and `end` or `limit` (an X-Total-Count header is included in the response)

```
GET /posts?start=20&end=30
GET /posts/1/comments?start=20&end=30
GET /posts/1/comments?start=20&limit=10
Works exactly as Array.slice (i.e. start is inclusive and end exclusive)
```
### Operators

Add `_gte` or `lte` for getting a range: `GET /posts?views_gte=10&views_lte=20`

Add `_ne` to exclude a value: `GET /posts?id_ne=1`

Add `_like` to filter (RegExp supported): `GET /posts?title_like=server`

Add `q` for full-text search: `GET /posts?q=internet`

## Information Object Model

The ‘objects’ used in APIs needs to be aligned with each other, so consumers request and receives the same structured information object from different APIs. A full list of the objects with attributes should be described and presented to internal/external developers.  

### Objects 

As an illustration, a number of objects that may be added to this set is listed below. This is not to be taken as an exhaustive set.

- All Parties (consignor,consignee, delivery, etc.)
- Address
- Shipment
- Order
- Service-points
- Location
- Events

### Object Attributes

The initially available attributes are briefly described below.                                     

#### Country attribute

All operations and services that requires a **Country** attribute, must use the **2-letter representation** in the [ISO 3166-1 alpha 2 standard](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2). 

#### Calendar day attribute

All operations and services that requires a **CalendarDay** attribute; must use the following enumerations. 

- **Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday**

#### Locale attribute

All operations and APIs that requires a **Locale** attribute, must use IETF language tags, as defined in RFC 5646. Example; 

| **Locale** | **Language** | **Country**   |
| ---------- | ------------ | ------------- |
| **da-DK**  | Danish       | Denmark       |
| **en-US**  | English      | United States |
| **sv-SE ** | Swedish      | Sweden        |

#### Currency attribute

All operations and services that requires a **Currency** attribute, must use the 3-letter representation in the [ISO 4217 standard](https://www.currency-iso.org/en/home.html)

#### Amount attribute

All operations and services that requires an **Amount **attribute; the “value”must be represented in a main unit base, and a subunit fraction of the main unit. The subunit fraction for the currency is given in the [ISO 4217 standard](https://www.currency-iso.org/en/home.html). The subunit is an integral power of 10.

- 100 SEK equals; Main unit base of: 10000 & Subunit of 100

> Representing money as a double or float will not work because it’s is not an exact representation of the value. Calculations will introduce rounds off errors.

